package ru.crystals.robotfx;

import java.util.Random;

import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseButton;

import org.hamcrest.Matcher;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.loadui.testfx.GuiTest;
import org.loadui.testfx.utils.FXTestUtils;

import ru.crystals.start.components.touch.calculator.CalculatorApp;

public class FirstTest {
	private Random random = new Random();
	private static GuiTest controller;
	

	@BeforeClass
	public static void setUpClass() {
		CalculatorApp.onLoad(r -> {
			controller = new GuiTest() {
				@Override
				protected Parent getRootNode() {
					return r;
				}
			};
		});

		FXTestUtils.launchApp(CalculatorApp.class);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Before
	public void beforeTest() {
		clear();
	}

	@Test
	public void testADD() throws InterruptedException {
		int digit1 = random.nextInt(1000);
		int digit2 = random.nextInt(1000);

		click(digit1);
		checkDescriptionField(String.valueOf(digit1));
		checkInputField(String.valueOf(digit1));

		perform(Operation.ADD);

		click(digit2);
		checkDescriptionField(digit1 + " + " + digit2);
		checkInputField(String.valueOf(digit2));

		perform(Operation.EQ);

		checkInputField(String.valueOf(digit1 + digit2) + ",00");
	}
	
	@Test
	public void testSubstract() throws InterruptedException {
		int digit1 = random.nextInt(1000);
		int digit2 = random.nextInt(1000);

		click(digit1);
		checkDescriptionField(String.valueOf(digit1));
		checkInputField(String.valueOf(digit1));

		perform(Operation.SUBTRACT);

		click(digit2);
		checkDescriptionField(digit1 + " − " + digit2);
		checkInputField(String.valueOf(digit2));

		perform(Operation.EQ);

		checkInputField(String.valueOf(digit1 - digit2) + ",00");
	}

	private void perform(Operation operation) {
		Matcher<Node> matcher = new OperationMatcher(operation);
		controller.click(matcher, MouseButton.PRIMARY);
	}

	public void checkDescriptionField(String expectedText)	throws InterruptedException {
		Thread.sleep(300);
		Node result = controller.find(".operation");
		String actualText = ((Labeled) result).getText();
		Assert.assertEquals(expectedText.trim(), actualText.trim());
	}

	public void checkInputField(String expectedText) throws InterruptedException {
		Thread.sleep(300);
		Node result = controller.find(".input");
		String actualText = ((TextField) result).getText();
		Assert.assertEquals(expectedText.trim(), actualText.trim());
	}

	public void click(int digit) {
		String numStr = Integer.toString(digit);
		for (int i = 0; i < numStr.length(); i++) {
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			controller.click(String.valueOf(numStr.charAt(i)));
		}
	}

	private void clear() {
		controller.click("УДАЛ.");
		
	}

}
