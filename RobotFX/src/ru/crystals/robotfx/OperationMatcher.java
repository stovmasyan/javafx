package ru.crystals.robotfx;

import javafx.scene.Node;
import javafx.scene.control.Labeled;

import org.hamcrest.Description;
import org.hamcrest.Matcher;

public class OperationMatcher implements Matcher<Node> {
	private Operation operation;

	public OperationMatcher(Operation operation) {
		this.operation = operation;
	}

	@Override
	public boolean matches(Object item) {
		if (item instanceof Labeled) {
			String expected = operation.toString().toLowerCase();
			String id = ((Labeled)item).getId();
			if (id != null) {
				if (expected.equals(id.toLowerCase())) {
					return true;
				}
			}
			
		}
		return false;
	}

	@Override
	public void describeTo(Description description) {
		// TODO Auto-generated method stub

	}

	@Override
	public void _dont_implement_Matcher___instead_extend_BaseMatcher_() {
		// TODO Auto-generated method stub

	}

	@Override
	public void describeMismatch(Object item, Description mismatchDescription) {
		// TODO Auto-generated method stub

	}

}
