package ru.crystals.robotfx;

public enum Operation {
	ADD,
    SUBTRACT,
    MULTIPLY,
    DIVIDE,
    EQ;
}
